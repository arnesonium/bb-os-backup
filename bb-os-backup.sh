#!/bin/bash
#
# Backup all Git repositories to S3. This will first GC all git repositories.

S3CMD=/usr/bin/s3cmd
REPODIR=/home/git/repositories

for d in $REPODIR/*.git
do
    pushd $d
    git gc
    popd
done

# The -rr uses reduced redundancy storage. Saves money!
$S3CMD sync -rr --delete-removed $REPODIR s3://arnesonium-backups/git/

